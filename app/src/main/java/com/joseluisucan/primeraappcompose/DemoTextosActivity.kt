package com.joseluisucan.primeraappcompose

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext

class DemoTextosActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

        }
    }
}

@Composable
fun Contenido(){
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(8.dp)) {
        /*Text(text = "Texto Simple")
        Spacer(modifier = Modifier.height(10.dp))
        TextoBold()
        Spacer(modifier = Modifier.height(10.dp))
        TextoCuriva()
        Spacer(modifier = Modifier.height(10.dp))
        TextoSubrayado()*/
        /*EditorText()
        Spacer(modifier = Modifier.height(10.dp))
        EditorTextoOutline()*/
        /*Boton()
        Spacer(modifier = Modifier.height(10.dp))
        OutlineBoton()*/
        BotonGenericoUno()
        Spacer(modifier = Modifier.height(10.dp))
        BotonGenericoDos()
        Spacer(modifier = Modifier.height(10.dp))
        BotonGenericoTres()
    }
}

@Composable
fun BotonGenericoUno(){
    val contexto = LocalContext.current
    val evento = { Toast.makeText(contexto, "Hola como estás?", Toast.LENGTH_SHORT).show()}
    BotonGenerico(textoBoton = "Saludar",evento)
}

@Composable
fun BotonGenericoDos(){
    val contexto = LocalContext.current
    val evento = { Toast.makeText(contexto, "Buscando...", Toast.LENGTH_SHORT).show()}
    BotonGenericoIcon(
        icono = Icons.Filled.Search,
        textoBoton = "Buscar",
        evento)
}

@Composable
fun BotonGenericoTres(){
    val contexto = LocalContext.current
    val evento = { Toast.makeText(contexto, "Llamando...", Toast.LENGTH_SHORT).show()}
    BotonGenericoIcon(
        icono = Icons.Filled.Phone,
        textoBoton = "Llamar",
        evento)
}

@Composable
fun Boton(){
    val contexto = LocalContext.current
    Button(
        onClick = { Toast.makeText(contexto,"Boton normal",Toast.LENGTH_SHORT).show()}
    ){
        Text(text = "Boton normal")
    }
}

@Composable
fun OutlineBoton(){
    val contexto = LocalContext.current
    OutlinedButton(
        onClick = {Toast.makeText(contexto,"Boton Outlined",Toast.LENGTH_SHORT).show()}
    ){
        Text(text = "Botón Outlined")
    }
}

@Composable
fun EditorText(){
    var text by remember { mutableStateOf("")}
    TextField(
        value = text,
        onValueChange = { text = it},
        label = {Text("Nombre")},
        placeholder = {Text(text="Ingresa tu nombre")}
    )
}

@Composable
fun EditorTextoOutline(){
    var text by remember { mutableStateOf("")}
    OutlinedTextField(
        value = text,
        onValueChange = { text = it},
        label = {Text("Nombre")},
        placeholder = {Text(text="Ingresa tu nombre")}
    )
}

@Preview(showBackground = true)
@Composable
fun PreviewContenido(){
    Contenido()
}

@Composable
fun TextoBold(){
    Text(text = "Texto Bold", fontWeight = FontWeight.Bold)
}

@Composable
fun TextoCuriva(){
    Text(text = "Texto Cursiva", style = TextStyle(fontFamily = FontFamily.Cursive, color = Color.Magenta))
}

@Composable
fun TextoSubrayado(){
    Text(text = "Texto Subrayado", style = TextStyle(textDecoration = TextDecoration.Underline))
}




