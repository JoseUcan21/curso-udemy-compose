package com.joseluisucan.primeraappcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.runtime.getValue
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue

class EstadosActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

        }
    }
}

@Preview
@Composable
fun MiSaveable(){
    var texto by rememberSaveable{ mutableStateOf("")}
    TextField(
        value = texto,
        onValueChange = { texto = it}
    )
}

@Preview
@Composable
fun MiByRemember(){
    var texto by remember{ mutableStateOf("")}
    TextField(
        value = texto,
        onValueChange = { texto = it}
    )
}

@Preview
@Composable
fun MiRemember() {
    val texto = remember{ mutableStateOf("")}
    TextField(
        value = texto.value,
        onValueChange = { texto.value = it}
    )
}

