package com.joseluisucan.primeraappcompose

import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector

@Composable
fun BotonGenerico(textoBoton:String,evento:()->Unit){
    Button(
        onClick = evento
    ){
        Text(text = textoBoton)
    }
}

@Composable
fun BotonGenericoIcon(icono:ImageVector,textoBoton: String,evento: () -> Unit){
    Button(
        onClick = evento
    ){
        Icon(imageVector = icono, contentDescription = null)
        Text(text = textoBoton)
    }
}