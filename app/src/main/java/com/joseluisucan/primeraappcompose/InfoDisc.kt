package com.joseluisucan.primeraappcompose

class InfoDisc {
    lateinit var title:String
    lateinit var subtitle:String
    var image = R.drawable.portada_linkin

    fun getAll(): MutableList<InfoDisc> {
        val lista = mutableListOf<InfoDisc>()
        repeat(20){
            val item = InfoDisc().apply {
                title = "Title $it"
                subtitle = "Subtitle $it"
            }
            lista.add(item)
        }
        return lista

    }
}