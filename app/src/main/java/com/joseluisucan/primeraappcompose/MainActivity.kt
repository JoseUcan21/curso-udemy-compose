package com.joseluisucan.primeraappcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.joseluisucan.primeraappcompose.ui.theme.PrimeraAppComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LinearVertical()
        }
    }
}

@Composable
fun LinearVertical(){
    Column(modifier = Modifier.padding(16.dp)) {
        Text(text = "Item 1")
        Text(text = "Item 2")
        Text(text = "Item 3")
        Text(text = "Item 4")
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewLinearVertical(){
    LinearVertical()
}

@Composable
fun LinearHorizontal(){
    Row(modifier = Modifier.padding(16.dp)) {
        Text(text = "Item 1")
        Text(text = "Item 2")
        Text(text = "Item 3")
        Text(text = "Item 4")
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewLinearHorizontal(){
    LinearHorizontal()
}

@Composable
fun FrameLayoutTest(){
    Box(modifier = Modifier
        .padding(4.dp)
        .fillMaxSize()){
        Text(text = "TS", modifier = Modifier.align(Alignment.TopStart))
        Text(text = "TC", modifier = Modifier.align(Alignment.TopCenter))
        Text(text = "TE", modifier = Modifier.align(Alignment.TopEnd))
        Text(text = "CS", modifier = Modifier.align(Alignment.CenterStart))
        Text(text = "Center", modifier = Modifier.align(Alignment.Center))
        Text(text = "CE", modifier = Modifier.align(Alignment.CenterEnd))
        Text(text = "BS", modifier = Modifier.align(Alignment.BottomStart))
        Text(text = "BC", modifier = Modifier.align(Alignment.BottomCenter))
        Text(text = "BE", modifier = Modifier.align(Alignment.BottomEnd))
    }
}

@Preview(showBackground = true)
@Composable
fun FrameLayoutPreview(){
    FrameLayoutTest()
}