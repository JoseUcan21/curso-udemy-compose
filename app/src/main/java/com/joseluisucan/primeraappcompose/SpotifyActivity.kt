package com.joseluisucan.primeraappcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue

class SpotifyActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SpotifyContent()
        }
    }
}

@Preview
@Composable
fun SpotifyContent(){
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        backgroundColor = Color.Black,
        topBar = { TopBar()},
        content = {
            Column(modifier = Modifier.fillMaxSize()) {
                DiscContent()
                SearchContent()
                PopulairContent()
            }
        }
    )
}

@Composable
fun TopBar(){
    TopAppBar(
        backgroundColor = Color.Black,
        title = {},
        navigationIcon = {
            IconButton(onClick = {}) {
                Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "Back", tint = Color.White)
            }
        },
        actions = {
            IconButton(onClick = {}) {
                Icon(imageVector = Icons.Filled.MoreVert, contentDescription = "More", tint = Color.White)
            }
        }
    )
}

@Composable()
fun DiscContent(){
    val imagen = painterResource(id = R.drawable.portada_linkin)
    Box(modifier = Modifier.fillMaxWidth()){
        Image(painter = imagen, contentDescription = "Portada", modifier = Modifier
            .fillMaxWidth()
            .height(350.dp))
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.BottomStart)
                .padding(8.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ){
            DiscInformation()
            ActionButtons()
        }
    }
}

@Composable
fun DiscInformation(){
    Column() {
        Text(text = "Linkin park",fontSize= 30.sp, fontWeight = FontWeight.Bold,color = Color.White)
        Spacer(modifier = Modifier.height(4.dp))
        Row{
            Text(text = "51.400.000", fontWeight = FontWeight.Bold,color = Color.Green)
            Spacer(modifier = Modifier.width(4.dp))
            Text(text = "Monthly listeners", fontWeight = FontWeight.Bold,color = Color.LightGray)
        }
        Spacer(modifier = Modifier.height(4.dp))
        OutlinedButton(
            onClick = {},
            border = BorderStroke(1.dp,Color.Green),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color.Transparent,
                contentColor = Color.Green
            )
        )
        {
            Text(text = "Follow", fontWeight = FontWeight.Bold)
        }
    }

}

@Composable
fun ActionButtons(){
    Box{
        FloatingActionButton(
            onClick = {},
            backgroundColor = Color.Green,
            contentColor = Color.White,
            modifier = Modifier
                .align(Alignment.Center)
                .size(60.dp)
        ) {
            Icon(imageVector = Icons.Default.PlayArrow, contentDescription = "Play")
        }

        FloatingActionButton(
            onClick = {},
            backgroundColor = Color.White,
            contentColor = Color.Green,
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .size(25.dp)
        ) {
            Icon(imageVector = Icons.Default.List, contentDescription = "List")
        }
    }
}

@Composable
fun SearchContent(){
    var texto by remember{ mutableStateOf("")}
    Column(modifier = Modifier
        .fillMaxWidth()
        .padding(16.dp)) {
        TextField(
            value = texto,
            onValueChange = { texto = it},
            leadingIcon = {Icon(imageVector = Icons.Default.Search, contentDescription = "Search", tint = Color.Green)},
            placeholder = { Text("Search this artist...",color=Color.Green)},
            modifier = Modifier.align(Alignment.CenterHorizontally),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.DarkGray, textColor = Color.Green, cursorColor = Color.Green
            )
        )
        Spacer(modifier = Modifier.height(24.dp))
        Text(text = "Populair", fontSize = 18.sp, fontWeight = FontWeight.Bold,color=Color.White)
    }
}

@Composable
fun PopulairContent(){
    LazyColumn(modifier = Modifier.fillMaxWidth()){
        items(InfoDisc().getAll()){
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)){
                Image(painter = painterResource(id = it.image), contentDescription = "", modifier = Modifier.size(40.dp).weight(1f))
                Column(modifier = Modifier.weight(3f)) {
                    Text(text = it.title, fontSize = 15.sp, fontWeight = FontWeight.Bold,color = Color.White)
                    Text(text = it.subtitle,  fontWeight = FontWeight.Bold,color = Color.LightGray)
                }
                Icon(imageVector = Icons.Default.ArrowForward, contentDescription = "", tint = Color.White,modifier = Modifier.weight(1f))
            }
        }
    }
}





























