package com.joseluisucan.primeraappcompose

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.joseluisucan.primeraappcompose.ui.theme.PrimeraAppComposeTheme

class WidgetsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

        }
    }
}

@Preview
@Composable
fun MiLazyRow(){
    val elementos = mutableListOf<String>()
    repeat(200){
        elementos.add("Item número $it")
    }

    LazyRow(modifier = Modifier.fillMaxSize()){
        items(elementos){ texto->
            Text(text = texto, modifier = Modifier.clickable {  })
            Spacer(modifier = Modifier.width(4.dp))
        }
    }
}


@Preview
@Composable
fun MiLazyColumnDos(){
    val elementos = mutableListOf<String>()
    repeat(200){
        elementos.add("Item número $it")
    }

    LazyColumn(modifier = Modifier.fillMaxSize()){
        items(elementos){ texto->
            Text(text = texto, modifier = Modifier.clickable {  })
            Spacer(modifier = Modifier.height(4.dp))
        }
    }
}

@Composable
fun MiLazyColumn(){
    LazyColumn(modifier = Modifier.fillMaxSize()){
        item{
            Text(text = "Primer elemento")
            Spacer(modifier = Modifier.height(4.dp))
        }
        items(100){index->
            Text(text = "Elemento número $index")
            Spacer(modifier = Modifier.height(4.dp))
        }
        item{
            Text(text = "Último elemento")
            Spacer(modifier = Modifier.height(4.dp))
        }
    }
}

@Composable
fun MiDialogo(){
    Column() {
        val mostrarDialogo = remember{ mutableStateOf(false)}
        Button(onClick = { mostrarDialogo.value = true }) {
            Text(text = "Mostrar Alert Dialog")
        }
        if(mostrarDialogo.value){
            AlertDialog(
                onDismissRequest = { mostrarDialogo.value = false },
                title = { Text(text = "Advertencia")},
                text = { Text(text = "Seguro que deseas continuar?")},
                confirmButton = {
                    Button(onClick = { mostrarDialogo.value = false }) {
                        Text(text = "Aceptar")
                    }
                },
                dismissButton = {
                    Button(onClick = { mostrarDialogo.value = false }) {
                        Text(text = "Cancelar")
                    }
                }
            )
        }
    }
}

@Composable
fun MiSnackBar(){
    Column(modifier = Modifier.padding(8.dp)) {
        val snackBarVisible = remember{ mutableStateOf(false)}

        Button(onClick = {
            snackBarVisible.value = !snackBarVisible.value
        }) {

            if(snackBarVisible.value){
                Text(text = "Ocultar snack Bar")
            }else{
                Text(text = "Mostrar snack Bar")
            }
        }

        if(snackBarVisible.value){
            Snackbar(
                action = {
                    Button(onClick = { /*TODO*/ }) {
                        Text("Action")
                    }
                }
            ) {
                Text(text = "Esto es un SnackBar")
            }
        }
    }
}

@Composable
fun MiRadioButton(){
    val radioButtons = listOf("opción 1","opción 2")
    val selected = remember{ mutableStateOf(radioButtons.first())}
    val colors = RadioButtonDefaults.colors(
        selectedColor = MaterialTheme.colors.primary,
        unselectedColor = MaterialTheme.colors.secondary,
        disabledColor = Color.LightGray
    )

    Row{
        radioButtons.forEach {
            val isSelected = it == selected.value
            RadioButton(
                colors = colors,
                selected = isSelected ,
                onClick = {
                    selected.value = it
                    Log.d("Radio","Seleccionaste $it")
                })
            Spacer(modifier = Modifier.width(4.dp))
            Text(text = it)
        }
    }
}

@Composable
fun MiCheckbox(){
    val isChecked = remember { mutableStateOf(false)}

    Row(
        modifier = Modifier
            .padding(16.dp)
            .clickable { isChecked.value = !isChecked.value }
    ){
        Checkbox(
            checked = isChecked.value,
            onCheckedChange = {isChecked.value = it}
        )
        Text(text="Mi checkbox")
    }
}
@Composable
fun CardExample(){
    val imagen = painterResource(id = R.drawable.ic_android)
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clickable { Log.d("compose", "Ejemplo de card") },
        backgroundColor = Color.LightGray,
        contentColor = Color.Magenta,
        shape = RoundedCornerShape(10.dp),
        elevation = 8.dp
    ){
        Column() {
            Image(painter = imagen, contentDescription = "Imagen Perfil", modifier = Modifier.fillMaxWidth())
            Text(text = "Card Example", modifier = Modifier.align(Alignment.CenterHorizontally),style = TextStyle(fontSize = 25.sp))
            Row(modifier = Modifier.align(Alignment.Start)){
                IconButton(onClick = { Log.d("compose","Favorite") }) {
                    Icon(Icons.Default.Favorite, contentDescription = null)
                }
                IconButton(onClick = { Log.d("compose","Share") }) {
                    Icon(Icons.Default.Share, contentDescription = null)
                }
            }
        }
    }
}

/*
@Preview(showBackground = true)
@Composable
fun CardExamplePreview(){
    CardExample()
}*/
